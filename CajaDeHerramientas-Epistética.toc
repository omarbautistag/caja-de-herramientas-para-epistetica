\babel@toc {spanish}{}
\contentsline {section}{\numberline {1}Introducción}{2}{section.1}%
\contentsline {section}{\numberline {2}Uso básico del lenguaje}{3}{section.2}%
\contentsline {section}{\numberline {3}Falacias informales}{3}{section.3}%
\contentsline {subsection}{\numberline {3.1}Falacias de atinencia}{3}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Falacias de ambigüedad}{4}{subsection.3.2}%
\contentsline {section}{\numberline {4}Definición}{5}{section.4}%
\contentsline {subsection}{\numberline {4.1}Definición por género y diferencia}{6}{subsection.4.1}%
\contentsline {section}{\numberline {5}Proposiciones categóricas}{6}{section.5}%
\contentsline {subsection}{\numberline {5.1}Cantidad, cualidad y distribución}{6}{subsection.5.1}%
\contentsline {subsection}{\numberline {5.2}Cuadrado de oposición clásico o aristotélico}{6}{subsection.5.2}%
\contentsline {subsection}{\numberline {5.3}Cuadrado de oposición moderno o booleano}{7}{subsection.5.3}%
\contentsline {subsection}{\numberline {5.4}Otras inferencias inmediatas}{7}{subsection.5.4}%
\contentsline {section}{\numberline {6}Silogismos categóricos}{7}{section.6}%
\contentsline {section}{\numberline {7}Argumentos en el lenguaje ordinario}{8}{section.7}%
\contentsline {subsection}{\numberline {7.1}Entimemas}{8}{subsection.7.1}%
\contentsline {subsection}{\numberline {7.2}Sorites}{8}{subsection.7.2}%
\contentsline {subsection}{\numberline {7.3}Silogismos disyuntivo e hipotético}{8}{subsection.7.3}%
\contentsline {subsection}{\numberline {7.4}Traducciones del lenguaje ordinario y modelos mentales silogísticos}{9}{subsection.7.4}%
\contentsline {section}{\numberline {8}Lógica simbólica}{9}{section.8}%
\contentsline {section}{\numberline {9}El método de la deducción}{9}{section.9}%
\contentsline {subsection}{\numberline {9.1}Reglas de inferencia}{10}{subsection.9.1}%
\contentsline {subsection}{\numberline {9.2}Reglas de reemplazo}{10}{subsection.9.2}%
\contentsline {subsection}{\numberline {9.3}Prueba de invalidez}{10}{subsection.9.3}%
\contentsline {subsection}{\numberline {9.4}Inconsistencia}{11}{subsection.9.4}%
\contentsline {section}{\numberline {10}Teoría de la cuantificación}{11}{section.10}%
\contentsline {subsection}{\numberline {10.1}Reglas de cuantificación}{11}{subsection.10.1}%
\contentsline {subsection}{\numberline {10.2}Inferencia ``asilogística''}{12}{subsection.10.2}%
\contentsline {section}{\numberline {11}Analogía e inferencia probable}{12}{section.11}%
\contentsline {subsection}{\numberline {11.1}Evaluación de argumentos analógicos}{12}{subsection.11.1}%
\contentsline {section}{\numberline {12}Conexiones causales: los métodos de Mill como referentes del diseño experimental}{12}{section.12}%
\contentsline {subsection}{\numberline {12.1}Método de concordancia}{13}{subsection.12.1}%
\contentsline {subsection}{\numberline {12.2}Método de la diferencia}{13}{subsection.12.2}%
\contentsline {subsection}{\numberline {12.3}Método conjunto de la concordancia y la diferencia}{13}{subsection.12.3}%
\contentsline {subsection}{\numberline {12.4}Método de los residuos}{13}{subsection.12.4}%
\contentsline {subsection}{\numberline {12.5}Método de la variación concomitante}{13}{subsection.12.5}%
\contentsline {subsection}{\numberline {12.6}Cuidados con los métodos de Mill}{13}{subsection.12.6}%
\contentsline {section}{\numberline {13}Integraciones difusas y polivalentes al sistema deductivo de las ciencias sociales}{13}{section.13}%
\contentsline {section}{\numberline {14}Ciencia e hipótesis}{13}{section.14}%
\contentsline {subsection}{\numberline {14.1}Evaluación de las explicaciones científicas}{13}{subsection.14.1}%
\contentsline {subsection}{\numberline {14.2}La aventura como ciencia}{14}{subsection.14.2}%
\contentsline {section}{\numberline {15}Probabilidad}{14}{section.15}%
\contentsline {subsection}{\numberline {15.1}Eventos simultáneos}{14}{subsection.15.1}%
\contentsline {subsection}{\numberline {15.2}Ocurrencias alternativas}{14}{subsection.15.2}%
\contentsline {subsection}{\numberline {15.3}Esperanza o valor esperado}{14}{subsection.15.3}%
\contentsline {section}{\numberline {16}La epistética en Psicología y en Sociología}{14}{section.16}%
\contentsline {subsection}{\numberline {16.1}Pensamiento y razonamiento}{15}{subsection.16.1}%
\contentsline {subsection}{\numberline {16.2}El razonamiento deductivo en Psicología}{15}{subsection.16.2}%
\contentsline {subsection}{\numberline {16.3}Razonamiento inductivo en Psicología}{15}{subsection.16.3}%
\contentsline {subsection}{\numberline {16.4}Estudio del discurso argumentativo}{16}{subsection.16.4}%
\contentsline {subsection}{\numberline {16.5}Discurso y arqueología}{16}{subsection.16.5}%
\contentsline {subsection}{\numberline {16.6}Argumentación razonable desde la Sociología}{17}{subsection.16.6}%
\contentsline {subsection}{\numberline {16.7}El uso metodológico de la lógica en sociología}{22}{subsection.16.7}%
\contentsline {subsection}{\numberline {16.8}La argumentación como instrumento de pensamiento y de razonamiento propositivo}{22}{subsection.16.8}%
\contentsline {section}{\numberline {17}Entre el tratamiento enunciativo y las maniobras estratégicas}{22}{section.17}%
