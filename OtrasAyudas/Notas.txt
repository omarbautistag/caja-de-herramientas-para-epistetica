Para correr bien las citas bibliográficas, y sus correspondientes referencias, es necesario ejecutar el comando siguiente:

$ bibtex CajaDeHerramientas-Epistética.aux

Antes de proceder con la compilación normal. Aún cuando se deba tomar en cuenta que hay que compilar con LaTeX-BibTeX.

Además de eso, deberíamos borrar todos los archivos que se crean (aunque no necesariamente el PDF), que acompañan a los principales, que son el TEX y el BIB. Es decir, hay que dejar que el error aparezca antes de proceder con la solución del BibTeX al archivo AUX. Dejando que aparezcan signos de interrogación en las citas, primero. Y finalmente proceder con la compilación especificada.
